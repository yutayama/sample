# Vitis HLS Reference

## HLS pragma

### interface

文法は以下の通り。必須なのは`mode`, `port` くらい??

```c
# pragma HLS interface \
    mode=<mode> \
    port=<name> \
    bundle=<string> \
    register \
    register_mode=<mode> \
    depth=<int> \
    offset=<string> \
    latency=<value> \
    clock=<string> \
    name=<string> \
    storage_type=<value> \
    num_read_outstanding=<int> \
    num_write_outstanding=<int> \
    max_read_burst_length=<int> \
    max_write_burst_length=<int>    
```

- `mode`: インタフェースの型を指定
    - `ap_none`: プロトコルなし
    - `ap_stable`: 
    - `s_axilite`: AXI4-Lite スレーブ
    - `m_axi`: AXI4 マスタ
    - `ap_ctrl_chain`: デフォルトではこれ

- `port`: pragmaを適用する関数引数 or 戻り値を指定. 戻り値の場合は `return` とする. 

- `bundle`: 同一のポートにまとめるグループを指定
    - 値は小文字で指定
    - AXI4ポートのPREFIXとなる。
        - `m_axi_<bundle_name>_*`: `mode=m_axi` の場合
        - `s_axi_<bundle_name>_*`: `mode=s_axilite` の場合

- `offset`: `mode=<s_axilite|m_axi>` の時のアドレスオフセット
    - `s_axilite` の場合 `<string>` でアドレスオフセットを指定
    - `m_axi` の場合 `<string>` に以下を指定
        - `off`: オフセットポートは生成しない
        - `direct`: スカラー入力のオフセットポートを生成
        - `slave`: オフセットポートを生成して AXI4-Lite Slave IFに自動的にマップ (デフォルト)

- `name`: 生成されるRTLでの変数名/ポート名
