# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
# set vitis_hls_proj "project_1";
# set vitis_hls_solu "solution_1";
# set ldflags "";
# set mflags "";

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

csim_design -ldflags ${ldflags} -mflags ${mflags}

exit
