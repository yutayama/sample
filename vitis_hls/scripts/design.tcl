# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
# set vitis_hls_proj "project_1";
# set vitis_hls_solu "solution_1";
# set xlnx_part_name "xck26-sfvc784-2LV-c";
# set clock_freq     "30MHz";
# set cflags         "-I./sources/design -I./sources/tb";
# set csimflags      "";
# set top            "axi_master";
# set design_dir     "sources";

open_project -reset ${vitis_hls_proj}
# flow_target は RTL設計でvivadoを使う場合は vivado, ソフトウェア開発のみでvitisを使う場合はvitis
open_solution -reset ${vitis_hls_solu} -flow_target vivado
#open_solution -reset ${vitis_hls_solu} -flow_target vitis

# Define technology and clock rate
set_part             "${xlnx_part_name}"
create_clock -period "${clock_freq}"

# Add design files
set srcs "";

if [expr ![info exists srcs] || {${srcs} eq ""}] {
  set srcs [list \
          [glob -nocomplain ${design_dir}/design/*.hpp] \
          [glob -nocomplain ${design_dir}/design/*.cpp]]
}

foreach file ${srcs} {
  puts "# add_file ${file}"
  set file "[file normalize "$file"]"
  add_files -cflags ${cflags} -csimflags ${csimflags} [list $file]
}

# Add test bench & files
set tbs "";

if [expr ![info exists tbs] || {${tbs} eq ""}] {
  set tbs [list \
          [glob -nocomplain ${design_dir}/tb/*.cpp]]
}

foreach file ${tbs} {
  puts "# add_tb ${file}"
  set file "[file normalize "$file"]"
  add_files -tb -cflags ${cflags} -csimflags ${csimflags} [list $file]
}

# Set the top-level function
set_top ${top}

# Set the export configuration
config_export \
    -description ${top} \
    -display_name ${top} \
    -format ip_catalog \
    -ipname ${top} \
    -rtl verilog
#   -library ${library} -vendor ${vendor} -version ${version}

exit
