# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
# set vitis_hls_proj "project_1";

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

config_export \
    -ipname ${top} \
    -description ${top} \
    -display_name ${top}
#   -library ${library} -vendor ${vendor} -version ${version}

# export_design: add <-flow syn|impl> option if trying RTL-synth or RTL-impl with routing
# export_design -flow impl -rtl verilog -format ip_catalog
export_design -flow impl

exit
