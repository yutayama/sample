# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
# set vitis_hls_proj "project_1";
# set vitis_hls_solu "solution_1";
# set ldflags "";
# set mflags "";
# set trace_level "none"; # none, all, port, port_hier

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

cosim_design -ldflags ${ldflags} -mflags ${mflags} \
    -coverage \
    -trace_level ${trace_level}

exit
