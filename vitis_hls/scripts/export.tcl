# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
# set vitis_hls_proj "project_1";

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

# export_design: add <-flow syn|impl> option if trying RTL-synth or RTL-impl with routing
export_design
# export_design -rtl verilog -format ip_catalog
# export_design -rtl verilog -format xo

exit
