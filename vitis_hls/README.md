# Vitis HLS

## Vitis HLS の GUI での実行

### Project 作成

### モジュールの作成

### テストベンチの作成と実行

### 高位合成の実行

### RTLのCoSIM

### IP の Export

## Vitis HLS の CUI (TCL) での実行


### Project と Solution の作成

Project を作成後に Solution を作成する。
GUIでは Project 作成時に自動で Solution も作成されるが, TCL ではそれぞれ作成する必要がある。

```tcl
source ./env.tcl

open_project -reset ${vitis_hls_proj}

set srcs "";

if [expr ![info exists srcs] || {${srcs} eq ""}] {
  set srcs [list \
          [glob -nocomplain ${design_dir}/design/*.hpp] \
          [glob -nocomplain ${design_dir}/design/*.cpp]]
}

if [expr ![info exists tbs] || {${tbs} eq ""}] {
  set tbs [list \
          [glob -nocomplain ${design_dir}/tb/*.cpp]]
}

# Add design files
foreach file ${srcs} {
  puts "# add_file ${file}"
  set file "[file normalize "$file"]"
  add_files -cflags ${cflags} -csimflags ${csimflags} [list $file]
}

# Add test bench & files
foreach file ${tbs} {
  puts "# add_tb ${file}"
  set file "[file normalize "$file"]"
  add_files -tb -cflags ${cflags} -csimflags ${csimflags} [list $file]
}

# Set the top-level function
set_top ${top}

exit
```

```tcl
source ./env.tcl
open_project ${vitis_hls_proj}
open_solution -reset ${vitis_hls_solu} -flow_target vitis

# Define technology and clock rate
set_part             "${xlnx_part_name}"
create_clock -period "${clock_freq}"

exit
```

### テストベンチの実行

```tcl
source ./env.tcl

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

csim_design -ldflags ${ldflags} -mflags ${mflags}

exit
```

### 高位合成

```tcl
source ./env.tcl

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

csynth_design

exit
```

### RTL CoSIM

```tcl
source ./env.tcl

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

cosim_design -ldflags ${ldflags} -mflags ${mflags} \
    -coverage \
    -trace_level ${trace_level}

exit
```

### IP の Export

```tcl
source ./env.tcl

open_project ${vitis_hls_proj}
open_solution ${vitis_hls_solu}

config_export \
    -ipname ${top} \
    -description ${top} \
    -display_name ${top}
#   -library ${library} -vendor ${vendor} -version ${version}

export_design -rtl verilog -format ip_catalog
export_design -rtl verilog -format xo

exit
```


## Vitis HLS で作成した IP を Vivado で使用

Vitis HLS で作成した IP を Vivado の IP Integrator で使う為には IP のリポジトリを登録する。

### GUIで登録

IPの追加の手順は以下の通り

- Main Window
    - Flow Navigator > PROJECT MANAGER > Settings をクリック
- Settings Window
    - Project Settings > IP を展開
    - Project Settings > IP > Repository を選択
    - IP > IP Repositories の + ボタンをクリック
- IP Repositories Window
    - 該当ディレクトリを選択して Select をクリック
- Settings Window
    - IP > IP Repositories に該当ディレクトリが追加されたことを確認して Apply をクリック
- Add Repository Window
    - 1 repository was added to the project と表示されるので OK をクリック

IPの確認は以下の通り

- Main Window
    - Flow Navigator > PROJECT MANAGER > IP Catalog をクリック
    - PROJECT MANAGER 右のウィンドウ > IP Catalog Tab > User Repository を展開
    - ... > User Repository > VITIS HLS IP を展開
    - 作成した IP が登録されているはず

### CUI(TCL)で登録

```tcl
set_property  ip_repo_paths  <hls_ip_path> [current_project]
update_ip_catalog
```

`<hls_ip_path>` は `<workdir>/<project>/<solution>/impl/ip` を指定する。
デフォルトの場合は `<workdir>/project_1/solution_1/impl/ip` となる。

## References

- [UG1399_Vitis_HLS_User_Guide]

[UG1399_Vitis_HLS_User_Guide]:https://docs.xilinx.com/r/ja-JP/ug1399-vitis-hls/Vitis-HLS-%E5%85%A5%E9%96%80