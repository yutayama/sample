/* -*- coding: utf-8 -*- */

#include "axi_master.hpp"

#include <stdio.h>
#include <string.h>

void axi_master(volatile int *a)
{

// depthをAXI-IFバッファのサイズ(BUF_SIZE)以下にすることが必要
// そうしないと CoSIM が SIGSEGV でエラーとなる
#pragma HLS INTERFACE m_axi port=a depth=32

  int i;
  int buff[BUF_SIZE];

  memcpy(buff, (const int*)a, BUF_SIZE * sizeof(int));

  for(i = 0; i < BUF_SIZE; i++){
    buff[i] = buff[i] + 100;
  }

  memcpy((int *)a, buff, BUF_SIZE * sizeof(int));

  return;
}
