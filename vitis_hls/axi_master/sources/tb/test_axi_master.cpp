/* -*- coding: utf-8 -*- */

#include "axi_master.hpp"

#include <stdio.h>

int main()
{
  int A[BUF_SIZE];
  int B[BUF_SIZE];

  //Put data into A
  for(int i = 0; i < BUF_SIZE; i++) {
    A[i] = i;
  }

  //Call the hardware function
  axi_master(A);

  //Run a software version of the hardware function to validate results
  for(int i = 0; i < BUF_SIZE; i++) {
    B[i] = i + 100;
  }

  //Compare results
  for(int i = 0; i < BUF_SIZE; i++) {
    if(B[i] != A[i]) {
      printf("FAIL::");
      printf("i = %d A = %d B = %d\n", i, A[i], B[i]);
      return 1;
    }
  }
  printf("PASS\n");
  return 0;
}
