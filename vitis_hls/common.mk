# -*- coding: utf-8 -*-

XILINX_HLS  ?= $(shell find /tools/Xilinx/Vitis_HLS -mindepth 1 -maxdepth 1 | sort | tail -n 1)
settings_sh  = $(XILINX_HLS)/.settings64-Vitis_HLS.sh
vitis_hls    = LIBRARY_PATH=/usr/lib/x86_64-linux-gnu vitis_hls
#vitis_hls    = vitis_hls

scripts_dir  = $(ROOT)/scripts
env_tcl      = env.tcl
project_obj  = $(PROJECT)/.proj
project_app  = $(PROJECT)/hls.app
solution_aps = $(PROJECT)/$(SOLUTION)/$(SOLUTION).aps
csim_exe     = $(PROJECT)/$(SOLUTION)/csim/build/csim.exe
csim_rpt     = $(PROJECT)/$(SOLUTION)/csim/report/$(TOP)_csim.log
csynth_rtl   = $(PROJECT)/$(SOLUTION)/syn/verilog/$(TOP).v
csynth_rpt   = $(PROJECT)/$(SOLUTION)/syn/report/$(TOP)_csynth.rpt
cosim_top    = $(PROJECT)/$(SOLUTION)/sim/verilog/$(TOP).v
cosim_rpt    = $(PROJECT)/$(SOLUTION)/sim/report/$(TOP)_cosim.rpt
ip_package   = $(PROJECT)/$(SOLUTION)/impl/export.zip
ip_repo      = $(PROJECT)/$(SOLUTION)/impl/ip
ip_repo_tcl  = $(TOP)_repo.tcl
export_rpt   = $(PROJECT)/$(SOLUTION)/impl/report/verilog/$(TOP)_export.rpt
synth_rpt    = $(PROJECT)/$(SOLUTION)/impl/report/verilog/export_syn.rpt
impl_rpt     = $(PROJECT)/$(SOLUTION)/impl/report/verilog/export_impl.rpt
SRCS        ?= 
TBS         ?= 

all: design

.PHONY: info clean distclean
.PHONY: design solution csim csynth cosim export synth impl launch

info:
	@echo $(XILINX_HLS)


env: $(env_tcl)
$(env_tcl):
	@touch $@
	@echo "set vitis_hls_proj \"project_1\";" >> $@
	@echo "set vitis_hls_solu \"solution_1\";" >> $@
	@echo "set xlnx_part_name \"xck26-sfvc784-2LV-c\";" >> $@
	@echo "set clock_freq     \"30MHz\";" >> $@
	@echo "set top            \"$(TOP)\";" >> $@
	@echo "set design_dir     \"sources\";" >> $@
	@echo "set srcs           \"$(SRCS)\";" >> $@
	@echo "set tbs            \"$(TBS)\";" >> $@
	@echo "set cflags         \"-I./sources/design -I./sources/tb\";" >> $@
	@echo "set csimflags      \"\";" >> $@
	@echo "set ldflags        \"\";" >> $@
	@echo "set mflags         \"\";" >> $@
	@echo "set trace_level    \"none\";" >> $@
 	# trace_level = none, all, port, or port_hier

design: $(project_app) $(solution_aps)
$(project_app) $(solution_aps): $(env_tcl)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/design.tcl
	touch $@

csim: $(csim_exe)
$(csim_exe): $(project_app)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/csim.tcl

csynth: $(csynth_rtl)
$(csynth_rtl): $(solution_aps)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/csynth.tcl

cosim: $(cosim_top)
$(cosim_top): $(csynth_rtl)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/cosim.tcl

export: $(ip_package)
$(ip_package): $(csynth_rtl)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/export.tcl

synth: $(synth_rpt)
$(synth_rpt): $(csynth_rtl)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/synth.tcl

impl: $(impl_rpt)
$(impl_rpt): $(csynth_rtl)
	. $(settings_sh) && \
 	$(vitis_hls) -f $(scripts_dir)/impl.tcl

ip_repo_tcl: $(ip_repo_tcl)
$(ip_repo_tcl): $(ip_repo)
	@touch $@
	@echo "set_property  ip_repo_paths  \"$(realpath $(ip_repo))\" [current_project];" >> $@
	@echo "update_ip_catalog;" >> $@

launch: $(project_app)
	. $(settings_sh) && \
	$(vitis_hls) -n -p $(project_app)

clean:
	$(RM) vitis_hls.log

distclean: clean
	$(RM) env.tcl
	$(RM) -r $(PROJECT)
