// -*- coding: utf-8; mode: c -*-

#include <stdio.h>

void hello();
void callback000_echo(void (*func)());
int callback000();

typedef void (*func_t)();
void callback001_echo(func_t);
int callback001();

int main(int argc, char** argv)
{
    void (*func)() = hello;
    callback000_echo(func);
    callback001_echo(func);
    return 0;
}

void hello()
{
    printf("hello");
    return;
}

void callback000_echo(void (*func)())
{
    func();
    printf("-");
    func();
    printf("\n");
}

int callback000()
{
    void (*func)() = hello;
    callback000_echo(func);
    return 0;
}

void callback001_echo(func_t func)
{
    func();
    printf("-");
    func();
    printf("\n");
}

int callback001()
{
    func_t func = hello;
    callback001_echo(func);
    return 0;
}
