# -*- coding: utf-8; mode: makefile -*-

OBJS	  = $(SRCS:%.c=%.o)
CC	      = gcc
CPPFLAGS += -Wall -g
#CFLAGS  +=
#LDFLAGS +=
#LDLIBS  +=

.PHONY: all run build distclean clean
all: run

run: $(TARGET)
	./$(TARGET)

build: $(TARGET)

distclean: clean
	$(RM) $(TARGET)

clean:
	$(RM) *.o

$(TARGET): $(OBJS)
	$(CC) -o $@ $^

.c.o:
	$(CC) $(CPPFLAGS) -c $<
