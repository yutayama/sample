// -*- coding: utf-8; mode: c -*-

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct Bitstream_s {
    uint8_t* buf;      //!< buffer
    uint32_t buf_size; //!< data length
    uint32_t wp;       //!< write pointer
    uint32_t rp;       //!< read pointer
    uint32_t sz;       //!< available bits
} Bitstream_t;

int Bitstream_initialize(Bitstream_t* bs, const uint32_t size);
int Bitstream_finalize(Bitstream_t* bs);
int Bitstream_write(Bitstream_t* bs, const uint32_t data, const uint32_t len);
int Bitstream_read(Bitstream_t* bs, uint32_t* data, const uint32_t len);

static uint32_t get_bits(const uint32_t buf, const int32_t lsb, const int32_t len);
static uint32_t set_bits(const uint32_t buf, const int32_t lsb, const int32_t len, const int32_t wdat);

int main(int argc, char** argv)
{
    uint8_t u8val[8] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
    uint32_t u32val = ((uint32_t*)&u8val[2])[0];
    uint32_t lsb = 4;
    uint32_t len = 6;
    uint32_t wdat = 0x11;
    uint32_t rd_val = get_bits(u32val, lsb, len);
    uint32_t wr_val = set_bits(u32val, lsb, len, wdat);
    printf("%08x\n", u32val);
    printf("%08x\n", rd_val);
    printf("%08x\n", wr_val);
    return 0;
}

int Bitstream_initialize(Bitstream_t* bs, const uint32_t size)
{
    bs->buf = malloc(sizeof(uint8_t) * size);
    bs->buf_size = size;
    bs->wp = 0;
    bs->rp = 0;
    bs->sz = 0;
    return 0;
}

int Bitstream_finalize(Bitstream_t* bs)
{
    free(bs->buf);
    bs->buf = 0;
    return 0;
}

int Bitstream_read(Bitstream_t* bs, uint32_t* data, const uint32_t len)
{
    if (bs->sz - len < 0) {
        return -1;
    }
    int i = bs->rp >> 3;
    int p = bs->rp & 0x7;
    uint32_t buf = ((uint32_t*)&(bs->buf[i]))[0];
    *data = get_bits(buf, p, len);
    bs->rp += len;
    bs->sz -= len;
    return 0;
}

int Bitstream_write(Bitstream_t* bs, uint32_t data, uint32_t len)
{
    if (bs->sz + len > bs->buf_size << 3) {
        return -1;
    }
    int i = bs->wp >> 3;
    int p = bs->wp & 0x7;
    uint32_t buf = ((uint32_t*)&(bs->buf[i]))[0];
    set_bits(buf, p, len, data);
    bs->wp += len;
    bs->sz += len;
    return 0;
}

uint32_t get_bits(const uint32_t buf, const int32_t lsb, const int32_t len)
{
    uint32_t mask = ((1 << len) - 1) << lsb;
    uint32_t val = (buf & mask) >> lsb;
    return val;
}

uint32_t set_bits(const uint32_t buf, const int32_t lsb, const int32_t len, const int32_t wdat)
{
    uint32_t mask = ((1 << len) - 1) << lsb;
    uint32_t val = (buf & ~mask) | (wdat << lsb);
    return val;
}
