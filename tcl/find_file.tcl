#!/usr/bin/env tclsh

puts [pwd]
set filelist [glob -nocomplain ./*.tcl];
foreach i $filelist {
    puts "file = $i";
}