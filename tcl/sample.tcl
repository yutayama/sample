# -*- coding: utf-8; mode: tcl -*-

set origin_dir "."
puts $origin_dir

set origin_dir_abs "[file normalize "$origin_dir"]"
puts $origin_dir_abs

set sourcedScript [ info script ]

set scriptAtInvocation $::argv0

puts $sourcedScript
puts "[file normalize "$scriptAtInvocation"]"
