# TCL

## Hello World

```
puts stdout "hello tcl"
puts stderr "Hello Tcl"
puts {hello tcl}
```

## 文法

TCLの文法は以下のようにコマンド + 引数を並べる。

```
command arg0 arg1 arg2
```

一行に複数のコマンドを指定する場合は`;`で繋げる。

```
command0 arg0; command1 arg1
```

複数行に1つのコマンドを指定する場合は行末に`\`を記述

```
command arg0 arg1 \
  arg2 arg3 \
  arg3 arg4 \
  ...
```

## 変数

TCLの変数のデータ型は全て文字列

```
set a 1
```

算術演算

```
expr 1 + 2
```

コマンドの置換

```
set mega [expr 1000 * 1000]
```

## 制御フロー

## 関数

## データ構造

## Command Reference

### file

https://www.freesoftnet.co.jp/webfiles/tclkits/doc/TclCmdRef/TclCmd/file_jp.htm