# Markdown

## サンプル

### リスト

- リスト
- リスト
    - リスト

### 番号付きリスト

1. 番号付きリスト
    1. 番号付きリスト
1. 番号付きリスト
    1. 番号付きリスト
    1. 番号付きリスト

### リンク

- [Google](https://google.com)
- [Amazon.co.jp]

[Amazon.co.jp]:https://amazon.co.jp

### 表

| a | b | c |
|---|---|---|
| 0 | 1 | 2 |

### 数式

$f(x) = x^2 + x$

$$
f(x, y) = x^2 + 2 xy + y^2
$$