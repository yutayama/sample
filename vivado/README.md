# Vivado

## What is this

Vivadoのコマンドライン向け合成環境

## Quick Start

標準の design (PS部のみ) の合成

```
make all
```

## How to use

make コマンドは以下の通り

- `make project` プロジェクトの生成 (プロジェクト名は`project_1`)
- `make launch` プロジェクトの起動
- `make design` Block Designの作成 (デザイン名は`design_1`)
- `make imple` Block Designの合成, Hardwareの生成 (ストリーム名は`project_1.xsa`)
- `make tcl` プロジェクトのtclファイル(`project_1.tcl`)の生成
- `make bd` `project_1.tcl` 内の procedure `cr_bd_design_1` のみの抽出 (`design/project_1_cr_bd_design.tcl`)

### 独自の設計に更新する場合

プロジェクトを作成してGUIを起動する。

```
make project
make launch
```

GUIでBlock Designの修正を行なう。

```
make bd
```

で `design/project_1_cr_bd_design.tcl` に `cr_bd_design_1` の定義ファイルが生成されるので、適当な名称(以下では`design/mydesign.tcl`)に変更。

Makefile, env.tcl の `design/cr_bd_design_1.tcl` を `design/mydesign.tcl` に変更。

※: `make` コマンドでの `env.tcl` 生成は今後対応予定

### 独自のRTLを追加する場合

今後追加予定

### 独自のIOピンの設定をする場合

`./design/constrs_1/*.xdc` を import する。

```tcl
# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties

foreach file [glob -nocomplain ./design/constrs_1/*.xdc] {
  set file "[file normalize "$file"]"
  set file_added [add_files -norecurse -fileset $obj [list $file]]
  set file "$file"
  set file [file normalize $file]
  set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
  set_property -name "file_type" -value "XDC" -objects $file_obj
}
```

## TIPS

### WARNING: [Vivado 12-8222]

以下のようなメッセージによりsynthやimplが失敗する場合はメモリ不足が原因の場合がある。

```
WARNING: [Vivado 12-8222] Failed run(s) : 'design_1_xbar_0_synth_1'
```

`project_1/project_1.runs/<module name>/runme.log` のログに以下のようなメッセージが出ている場合はメモリ不足によるSegmentation Faultで終了している。
https://support.xilinx.com/s/question/0D52E00006hpW1ISAU/vivado-synthesis-issue-rdiprog-?language=ja
```
/tools/Xilinx/Vivado/2022.1/bin/rdiArgs.sh: line 312:  9127 Killed                  "$RDI_PROG" "$@"
```

### WARNING: [BD 41-237]

`generate_target all` の追加で対応できている気がする。

https://support.xilinx.com/s/question/0D52E00006hpNhgSAE/warnings-bd-41237-and-bd-41237-during-block-design-validatrion?language=ja