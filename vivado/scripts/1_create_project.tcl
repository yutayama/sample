# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl

# ****************************************
# 1. Create project
# ****************************************

create_project \
    ${vivado_proj_name} \
    ./${vivado_proj_name} \
    -part ${xlnx_part_name}

# ****************************************
# 2. Set project properties
# ****************************************

set obj [current_project]
set_property -name "board_part" -value ${xlnx_board_part} -objects $obj

# ****************************************
# 3. Create Filesets
# ****************************************

# 3.1. Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# 3.2. Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# 3.3. Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# ****************************************
# 4. Create Design Runs
# ****************************************

# 4.1. Create 'synth_1' run
if {[string equal [get_runs -quiet synth_1] ""]} {
    create_run -name synth_1 \
        -part ${xlnx_part_name} \
        -flow ${vivado_synth_1_flow} \
        -strategy ${vivado_synth_1_strategy} \
        -report_strategy {No Reports} \
        -constrset constrs_1
} else {
  set_property flow ${vivado_synth_1_flow} [get_runs synth_1]
  set_property strategy ${vivado_synth_1_strategy} [get_runs synth_1]
}
# Set the current synth run
current_run -synthesis [get_runs synth_1]

# 4.2. Create 'impl_1'
if {[string equal [get_runs -quiet impl_1] ""]} {
    create_run -name impl_1 \
        -part ${xlnx_part_name} \
        -flow ${vivado_impl_1_flow} \
        -strategy ${vivado_impl_1_strategy} \
        -report_strategy {No Reports} \
        -constrset constrs_1 \
        -parent_run synth_1
} else {
  set_property flow ${vivado_impl_1_flow} [get_runs impl_1]
  set_property strategy ${vivado_impl_1_strategy} [get_runs impl_1]
}
# Set the current impl run
current_run -implementation [get_runs impl_1]

# save_project_as で明示しなくても保存される
#save_project_as ${vivado_proj_name} ./${xlnx_part_name} -force
