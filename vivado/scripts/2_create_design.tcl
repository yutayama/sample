# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl
source ${vivado_proj_block_design}

open_project ${vivado_proj_name}/${vivado_proj_name}.xpr

# ****************************************
# Add/Import Sources & Constrs
# ****************************************

# Set 'sources_1' fileset object
set obj [get_filesets sources_1]

# Add/Import sources file and set constrs file properties
foreach file [glob -nocomplain ${design_dir}/sources/*.v] {
  set file "[file normalize "$file"]"
  set file_added [add_files -norecurse -fileset $obj [list $file]]
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties
foreach file [glob -nocomplain ${design_dir}/constrs/*.xdc] {
  set file "[file normalize "$file"]"
  set file_added [add_files -norecurse -fileset $obj [list $file]]
  set file "$file"
  set file [file normalize $file]
  set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
  set_property -name "file_type" -value "XDC" -objects $file_obj
}

# ****************************************
# Create Block Design
# ****************************************

set design_file [file normalize ${vivado_proj_name}/${vivado_proj_name}.srcs/sources_1/bd/design_1/design_1.bd]
if { [file isfile ${design_file}] } {
  puts "INFO: [USER] Overwrite Block Design ${design_file}"
  remove_files  ${design_file}
}
cr_bd_design_1 ""
set_property REGISTERED_WITH_MANAGER "1" [get_files design_1.bd ] 
set_property SYNTH_CHECKPOINT_MODE "Hierarchical" [get_files design_1.bd ]

# ****************************************
# Create Wrapper
# ****************************************

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property -name "top" -value "design_1_wrapper" -objects $obj

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property -name "top" -value "design_1_wrapper" -objects $obj
set_property -name "top_lib" -value "xil_defaultlib" -objects $obj

#call make_wrapper to create wrapper files
set wrapper_path [make_wrapper -fileset sources_1 -files [ get_files -norecurse design_1.bd] -top]
add_files -norecurse -fileset sources_1 $wrapper_path
