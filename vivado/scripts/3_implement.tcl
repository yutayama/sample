# -*- coding: utf-8; mode: tcl -*-

source ./env.tcl

open_project ${vivado_proj_name}/${vivado_proj_name}.xpr

generate_target all [get_files  ${vivado_proj_name}/${vivado_proj_name}.srcs/sources_1/bd/design_1/design_1.bd]

# ****************************************
# Run Synthesis
# ****************************************
reset_runs synth_1
launch_runs synth_1 -job ${max_jobs}
wait_on_run synth_1

# ****************************************
# Run Implementation
# ****************************************
launch_runs impl_1 -job ${max_jobs}
wait_on_run impl_1
# open_run    impl_1

# ****************************************
# Write Bitstream File
# ****************************************
launch_runs impl_1 -to_step write_bitstream -job ${max_jobs}
wait_on_run impl_1

# ****************************************
# Export Hardware
# ****************************************
set design_top_name [get_property "top" [current_fileset]]
write_hw_platform -fixed -force -include_bit -file ${vivado_proj_name}.xsa

# ****************************************
# Close Project
# ****************************************

close_project
