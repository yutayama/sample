// -*- codnig: utf-8 -*-

module gpio_scan #(
    parameter DATA_WIDTH = 8
)
(
    input clock,
    input reset_n,
    input [DATA_WIDTH-1:0] data_i,
    output [DATA_WIDTH-1:0] data_o
);
    localparam D1 = 1;
    reg [DATA_WIDTH-1:0] r_data;
    wire [DATA_WIDTH-1:0] w_data;
    assign data_o = r_data;
    assign w_data = data_i;

    always @ (posedge clock) begin
        if (~reset_n) begin
            r_data <= #D1 {DATA_WIDTH{1'b0}};
        end
        else begin
            r_data <= #D1 w_data;
        end
    end

endmodule // gpio_scan
