// -*- codnig: utf-8 -*-

module gpio_iobuf #(
    parameter DATA_WIDTH = 8
)
(
    output [DATA_WIDTH-1:0] data_i,
    input  [DATA_WIDTH-1:0] data_o,
    input  [DATA_WIDTH-1:0] data_t,
    inout  [DATA_WIDTH-1:0] data_io
);

    genvar i;
    generate
        for (i = 0; i < DATA_WIDTH; i = i + 1) begin: generate_iobuf
            IOBUF iobuf (
                .O(data_i[i]),
                .I(data_o[i]),
                .T(data_t[i]),
                .IO(data_io[i])
            );
        end
    endgenerate

endmodule // gpio_scan
