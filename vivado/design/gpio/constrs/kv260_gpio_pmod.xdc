# Xilinx design constraints (XDC) file for Kria K26 SOM - Rev 1

set_property PACKAGE_PIN H12 [get_ports {gpio_rtl_tri_io[0]}]
set_property PACKAGE_PIN E10 [get_ports {gpio_rtl_tri_io[1]}]
set_property PACKAGE_PIN D10 [get_ports {gpio_rtl_tri_io[2]}]
set_property PACKAGE_PIN C11 [get_ports {gpio_rtl_tri_io[3]}]
set_property PACKAGE_PIN B10 [get_ports {gpio_rtl_tri_io[4]}]
set_property PACKAGE_PIN E12 [get_ports {gpio_rtl_tri_io[5]}]
set_property PACKAGE_PIN D11 [get_ports {gpio_rtl_tri_io[6]}]
set_property PACKAGE_PIN B11 [get_ports {gpio_rtl_tri_io[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {gpio_rtl_tri_io[7]}]

# set_property PACKAGE_PIN H12      [get_ports "som240_1_a17"] ;# Pmod Pin1 Bank  45 VCCO - som240_1_b13 - IO_L4N_AD12N_45
# set_property PACKAGE_PIN E10      [get_ports "som240_1_d20"] ;# Pmod Pin2 Bank  45 VCCO - som240_1_b13 - IO_L7P_HDGC_45
# set_property PACKAGE_PIN D10      [get_ports "som240_1_d21"] ;# Pmod Pin3 Bank  45 VCCO - som240_1_b13 - IO_L7N_HDGC_45
# set_property PACKAGE_PIN C11      [get_ports "som240_1_d22"] ;# Pmod Pin4 Bank  45 VCCO - som240_1_b13 - IO_L9P_AD11P_45
# set_property PACKAGE_PIN B10      [get_ports "som240_1_b20"] ;# Pmod Pin5 Bank  45 VCCO - som240_1_b13 - IO_L9N_AD11N_45
# set_property PACKAGE_PIN E12      [get_ports "som240_1_b21"] ;# Pmod Pin6 Bank  45 VCCO - som240_1_b13 - IO_L8P_HDGC_45
# set_property PACKAGE_PIN D11      [get_ports "som240_1_b22"] ;# Pmod Pin7 Bank  45 VCCO - som240_1_b13 - IO_L8N_HDGC_45
# set_property PACKAGE_PIN B11      [get_ports "som240_1_c22"] ;# Pmod Pin8 Bank  45 VCCO - som240_1_b13 - IO_L10P_AD10P_45
