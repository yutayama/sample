# -*- coding: utf-8; mode: tcl -*-

# Synthesis & Implementation Strategy
set vivado_synth_1_flow         "Vivado Synthesis 2021";
set vivado_synth_1_strategy     "Vivado Synthesis Defaults";
set vivado_impl_1_flow          "Vivado Implementation 2021";
set vivado_impl_1_strategy      "Vivado Implementation Defaults";
