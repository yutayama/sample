# Makefile

## 変数

- `$@`: ルールのターゲットの名前 (`$(@)`でもOK)
    - `$(@D)`: `$@`のディレクトリ名
    - `$(@F)`: `$@`ののファイル名
- `$<`: 依存関係の先頭の名前 (`$(<)`でもOK)
    - `$(<D)`: `$<`のディレクトリ名
    - `$(<F)`: `$<`ののファイル名
- `$^`: 依存関係の全ての名前 (`$(^)`でもOK)
    - `$(<D)`: `$^`のディレクトリ名
    - `$(<F)`: `$^`ののファイル名
- `$?`: **ターゲットよりタイムスタンプが新しい**依存関係の全ての名前 (`$(?)`でもOK)
    - `$(?D)`: `$?`のディレクトリ名
    - `$(?F)`: `$?`ののファイル名


## 標準コマンド一覧

- $(AR)         Archive-maintaining program; default ‘ar’.
- $(AS)         Program for compiling assembly files; default ‘as’.
- $(CC)         Program for compiling C programs; default ‘cc’.
- $(CXX)        Program for compiling C++ programs; default ‘g++’.
- $(CPP)        Program for running the C preprocessor, with results to standard output; default ‘$(CC) -E’.
- $(FC)         Program for compiling or preprocessing Fortran and Ratfor programs; default ‘f77’.
- $(M2C)        Program to use to compile Modula-2 source code; default ‘m2c’.
- $(PC)         Program for compiling Pascal programs; default ‘pc’.
- $(CO)         Program for extracting a file from RCS; default ‘co’.
- $(GET)        Program for extracting a file from SCCS; default ‘get’.
- $(LEX)        Program to use to turn Lex grammars into source code; default ‘lex’.
- $(YACC)       Program to use to turn Yacc grammars into source code; default ‘yacc’.
- $(LINT)       Program to use to run lint on source code; default ‘lint’.
- $(MAKEINFO)   Program to convert a Texinfo source file into an Info file; default ‘makeinfo’.
- $(TEX)        Program to make TeX DVI files from TeX source; default ‘tex’.
- $(TEXI2DVI)   Program to make TeX DVI files from Texinfo source; default ‘texi2dvi’.
- $(WEAVE)      Program to translate Web into TeX; default ‘weave’.
- $(CWEAVE)     Program to translate C Web into TeX; default ‘cweave’.
- $(TANGLE)     Program to translate Web into Pascal; default ‘tangle’.
- $(CTANGLE)    Program to translate C Web into C; default ‘ctangle’.
- $(RM)         Command to remove a file; default ‘rm -f’.

## ビルドオプション関連

- $(ARFLAGS)    Flags to give the archive-maintaining program; default ‘rv’.
- $(ASFLAGS)    Extra flags to give to the assembler (when explicitly invoked on a ‘.s’ or ‘.S’ file).
- $(CFLAGS)     Extra flags to give to the C compiler.
- $(CXXFLAGS)   Extra flags to give to the C++ compiler.
- $(COFLAGS)    Extra flags to give to the RCS co program.
- $(CPPFLAGS)   Extra flags to give to the C preprocessor and programs that use it (the C and Fortran compilers).
- $(FFLAGS)     Extra flags to give to the Fortran compiler.
- $(GFLAGS)     Extra flags to give to the SCCS get program.
- $(LDFLAGS)    Extra flags to give to compilers when they are supposed to invoke the linker, ‘ld’, such as -L. Libraries (-lfoo) should be added to the LDLIBS variable instead.
- $(LDLIBS)     Library flags or names given to compilers when they are supposed to invoke the linker, ‘ld’. LOADLIBES is a deprecated (but still supported) alternative to LDLIBS. Non-library linker flags, such as -L, should go in the LDFLAGS variable.
- $(LFLAGS)     Extra flags to give to Lex.
- $(YFLAGS)     Extra flags to give to Yacc.
- $(PFLAGS)     Extra flags to give to the Pascal compiler.
- $(RFLAGS)     Extra flags to give to the Fortran compiler for Ratfor programs.
- $(LINTFLAGS)  Extra flags to give to lint.

## 各種コマンド

- `.c.o`


## 各種TIPS

### 外部ファイルをsourceする場合

debianやubuntuの場合は標準では `/bin/sh` が `dash` なので makefile 中で `source` コマンドは実行できない。
以下の2つの対応のいずれかが必要

1. `sudo dpkg-reconfigure dash` を実行して NO を選択して `/bin/sh` を `bash` に変更
2. makefile 中では `source` ではなく `.` を使用
  - posix 的には `source` ではなく `.` を使う。
