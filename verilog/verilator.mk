# -*- coding: utf-8; mode: makefile -*-

verilator = $(shell which verilator)
VERILATOR_NO_WANING += --Wno-ASSIGNDLY

.PHONY: lint

sim: ./obj_dir/V$(TOP)
	./obj_dir/V$(TOP)

verilator: ./obj_dir/V$(TOP)
./obj_dir/V$(TOP): ./obj_dir/V$(TOP).mk
	make -C obj_dir -f V$(TOP).mk

lib: ./obj_dir/V$(TOP)__ALL.a
./obj_dir/V$(TOP)__ALL.a: ./obj_dir/V$(TOP).mk
	make -C obj_dir -f V$(TOP).mk

class: ./obj_dir/V$(TOP).mk
./obj_dir/V$(TOP).mk: $(SRCS_V) $(SRCS_SV) $(TB)
	$(verilator) --cc -Wall $(VERILATOR_NO_WANING) $(DESIGN_V) $(DESIGN_SV) --exe $(TB)

lint: $(SRCS_V) $(SRCS_SV)
	$(verilator) --lint-only -Wall $(VERILATOR_NO_WANING) $(DESIGN_V) $(DESIGN_SV)

clean:
	$(RM) -r ./obj_dir
