// -*- coding: utf-8 -*-

module env
#(parameter DATA_WIDTH = 8)
(
    buffer_wr_if.ini buffer_wr_if,
    buffer_rd_if.ini buffer_rd_if,
    input clock,
    input reset_n
);
    logic [DATA_WIDTH-1:0] data;
    initial begin
        @ (negedge reset_n);
        $display("# ---- Reset Start  ----#");
        buffer_wr_if.ini_reset();
        buffer_rd_if.ini_reset();
        @ (posedge reset_n);
        $display("# ---- Reset Finish ----#");
        $display("# ---- Test  Start  ----#");
        fork
            begin
                $display("send data = %x", 1);
                buffer_wr_if.send(1);
                @ (posedge clock);
                $display("send data = %x", 2);
                buffer_wr_if.send(2);
                @ (posedge clock);
                buffer_wr_if.send(3);
                @ (posedge clock);
                buffer_wr_if.send(4);
                @ (posedge clock);
                buffer_wr_if.send(5);
                @ (posedge clock);
                buffer_wr_if.send(6);
                @ (posedge clock);
                buffer_wr_if.send(7);
                @ (posedge clock);
                buffer_wr_if.send(8);
                @ (posedge clock);
            end
            begin
                forever begin
                    buffer_rd_if.recv(data);
                    $display("recv data = %x", data);
                end
            end
        join
        $display("# ---- Test  Finish ----#");
    end

endmodule // env