// -*- coding: utf-8 -*-

module clock_gen
#(parameter CLOCK_DELAY=8,
  parameter CLOCK_PERIOD=10)
(
    output logic clock
);

  initial begin
    clock = '0;
    #CLOCK_DELAY;
    forever #(CLOCK_PERIOD/2) clock = ~clock;
  end

endmodule // reset_gen
