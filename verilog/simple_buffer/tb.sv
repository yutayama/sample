// -*- coding: utf-8 -*-

module tb;

    parameter TIMEOUT = 1000;
    parameter DATA_WIDTH = 8;

    logic clock;
    logic reset;
    logic reset_n;

    logic                  wr_valid = '0;
    logic                  wr_ready;
    logic [DATA_WIDTH-1:0] wr_data = '0;
    logic                  rd_valid = '0;
    logic                  rd_ready;
    logic [DATA_WIDTH-1:0] rd_data;
    
    clock_gen m_clock_gen(.*);
    reset_gen m_reset_gen(.*);
    buffer_wr_if #(.DATA_WIDTH(DATA_WIDTH)) buffer_wr_if(.*);
    buffer_rd_if #(.DATA_WIDTH(DATA_WIDTH)) buffer_rd_if(.*);
    env #(.DATA_WIDTH(DATA_WIDTH)) env(
        .buffer_wr_if(buffer_wr_if.ini),
        .buffer_rd_if(buffer_rd_if.ini),
        .*
    );
    buffer #(.DATA_WIDTH(DATA_WIDTH)) dut(
        .wr_valid(buffer_wr_if.valid),
        .wr_ready(buffer_wr_if.ready),
        .wr_data (buffer_wr_if.data ),
        .rd_valid(buffer_rd_if.valid),
        .rd_ready(buffer_rd_if.ready),
        .rd_data (buffer_rd_if.data ),
        .*
        );

    initial begin
        #TIMEOUT $finish;
    end

    initial begin
        $display("HELLO SystemVerilog!!");
        forever begin
            @ (negedge clock);
            $write($time);
            $write("::wr");
            buffer_wr_if.display();
            $write(" - rd");
            buffer_rd_if.display();
            $write("\n");
        end
    end

endmodule
