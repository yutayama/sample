// -*- coding: utf-8 -*-

module reset_gen
#(parameter RESET_START=10,
  parameter RESET_PERIOD=10,
  parameter D1=1)
(
    input logic clock,
    output logic reset,
    output logic reset_n
);

  assign reset_n = ~reset;

  initial begin
    reset = '0;
    repeat (RESET_START) @ (posedge clock);
    #D1;
    reset = '1;
    repeat (RESET_PERIOD) @ (posedge clock);
    #D1;
    reset = '0;
  end

  initial begin
    $monitor ($time, "reset = %b, reset_n = %b", reset, reset_n);
  end

endmodule // reset_gen
