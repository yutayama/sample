// -*- coding: utf-8 -*-

interface buffer_rd_if
#(parameter DATA_WIDTH = 8)
(input clock);

    logic valid;
    logic ready;
    logic [DATA_WIDTH-1:0] data;

    function automatic display();
        $write("(v, r, d) = %x, %x, %x", valid, ready, data);
    endfunction

    function automatic is_done;
        return valid & ready;
    endfunction

    task automatic ini_reset();
        begin
            valid = 1'b0;
        end
    endtask // send

    task automatic recv(output [DATA_WIDTH-1:0] recv_data);
        begin
            valid = 1'b1;
            @ (posedge clock);
            while(!is_done()) @ (posedge clock);
            valid = 1'b0;
            recv_data = data;
        end
    endtask // send

    modport ini (
        import ini_reset,
        import recv,
        output valid,
        input  ready,
        input  data
    );

    modport tgt (
        input  valid,
        output ready,
        output data
    );

endinterface // buffer_rd_if

interface buffer_wr_if
#(parameter DATA_WIDTH = 8)
(input clock);

    logic valid;
    logic ready;
    logic [DATA_WIDTH-1:0] data;

    function automatic display();
        $write("(v, r, d) = %x, %x, %x", valid, ready, data);
    endfunction

    function automatic is_done;
        return valid & ready;
    endfunction

    task automatic ini_reset();
        begin
            valid = 1'b0;
            data = '0;
        end
    endtask // send

    task automatic send(input [DATA_WIDTH-1:0] send_data);
        begin
            valid = 1'b1;
            data = send_data;
            @ (posedge clock);
            while(!is_done()) @ (posedge clock);
            valid = 1'b0;
        end
    endtask // send

    modport ini (
        import ini_reset,
        import send,
        output valid,
        input  ready,
        output data
    );

    modport tgt (
        input  valid,
        output ready,
        input  data
    );

endinterface // buffer_wr_if
