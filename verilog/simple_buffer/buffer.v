// -*- coding: utf-8 -*-

module buffer
#(
    parameter DATA_WIDTH = 8,
    parameter D1 = 1
)
(
    input clock,
    input reset_n,
    input                   wr_valid,
    output                  wr_ready,
    input  [DATA_WIDTH-1:0] wr_data,
    input                   rd_valid,
    output                  rd_ready,
    output [DATA_WIDTH-1:0] rd_data
);

    reg                  r_wr_ready;
    reg                  r_rd_ready;
    reg [DATA_WIDTH-1:0] r_data;

    assign wr_ready = r_wr_ready;
    assign rd_ready = r_rd_ready;
    assign rd_data = r_data;

    always @ (posedge clock or negedge reset_n) begin
        if (~reset_n) begin
            r_wr_ready <= #D1 1'b1;
            r_rd_ready <= #D1 1'b0;
            r_data <= #D1 {DATA_WIDTH{1'b0}};
        end
        else if (wr_valid & r_wr_ready) begin
            r_wr_ready <= #D1 1'b0;
            r_rd_ready <= #D1 1'b1;
            r_data <= #D1 wr_data;
        end
        else if (rd_valid & r_rd_ready) begin
            r_wr_ready <= #D1 1'b1;
            r_rd_ready <= #D1 1'b0;
            // r_data <= #D1 {DATA_WIDTH{1'b0}};
        end
    end

endmodule // buffer
