# -*- coding: utf-8; mode: makefile -*-

DEBUG         ?= 0

XILINX_VIVADO ?= $(shell find /tools/Xilinx/Vivado -mindepth 1 -maxdepth 1 | tail -n 1)
cpu_cores      = $(shell grep cpu.cores /proc/cpuinfo | sort -u | awk -F':' '{print $$2}')
settings_sh    = $(XILINX_VIVADO)/.settings64-Vivado.sh
MAX_JOBS      ?= $(cpu_cores)

xvlog          = $(XILINX_VIVADO)/bin/xvlog
xelab          = $(XILINX_VIVADO)/bin/xelab
xsim           = $(XILINX_VIVADO)/bin/xsim

worklib        = worklib
objs           = $(addprefix ./xsim.dir/$(worklib)/,$(SRCS_V:%.v=%.sdb)) $(addprefix ./xsim.dir/$(worklib)/,$(SRCS_SV:%.sv=%.sdb))
xsimk          = ./xsim.dir/$(worklib).$(TOP)/xsimk
inc_opt        = $(addprefix -i ,$(INCDIR))

xvlog_opt     += $(inc_opt) 
xelab_opt     += $(inc_opt)

ifeq ($(USE_UVM),1)
uvm_opt        = -L uvm --uvm_version 1.2
xvlog_opt     += $(uvm_opt) 
xelab_opt     += $(uvm_opt)
  ifdef UVM_TESTNAME
  xsim_opt      += --testplusarg "UVM_TESTNAME=$(UVM_TESTNAME)"
  endif
endif

ifeq ($(DEBUG),0)
xsim_opt      += --runall
else
xelab_opt     += --debug all
xsim_opt      += --gui --wdb wave
endif

.PHONY: vlog elab sim help clean distclean

all: sim
vlog: $(objs)
elab: $(xsimk)
sim: $(xsimk)
	. $(settings_sh) && \
	$(xsim) $(worklib).$(TOP) $(xsim_opt)

./xsim.dir/$(worklib)/%.sdb: %.sv
	. $(settings_sh) && \
	$(xvlog) -work $(worklib) -sv $^ $(xvlog_opt)

./xsim.dir/$(worklib)/%.sdb: %.v
	. $(settings_sh) && \
	$(xvlog) -work $(worklib) $^

$(xsimk): $(objs)
	. $(settings_sh) && \
	$(xelab) $(worklib).$(TOP) $(xelab_opt)

help:
	@echo make <command>

info:
	@echo CPU_CORES: $(CPU_CORES)
	@echo $(objs)

clean:
	$(RM) -r xsim.dir/
	$(RM) xvlog.log xelab.log
	$(RM) xsim*.log xsim*.jou
	$(RM) xvlog.pb xelab.pb

distclean: clean
