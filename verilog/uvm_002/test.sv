// -*- coding: utf-8 -*-

`include "uvm_macros.svh"
import uvm_pkg::*;

class test extends uvm_test;

    `uvm_component_utils(test)
    virtual adder_if vif;

    function new(string name, uvm_component parent);
        super.new(name, parent);
        if (!uvm_config_db #(virtual adder_if)::get(this, "", "vif", vif))
            `uvm_fatal(this.get_name(), "vif is invalid")
    endfunction

    task run_phase(uvm_phase phase);
        int exp;
        `uvm_info(this.get_name(), "Hello UVM World::Test", UVM_NONE);
        fork
            begin
                `uvm_info(this.get_name(), "Hello UVM Test::Driver::Start", UVM_NONE);
                forever begin
                    vif.in_a = 1;
                    vif.in_b = 2;
                    @vif.clock;
                end
                `uvm_info(this.get_name(), "Hello UVM Test::Driver::Finish", UVM_NONE);
            end
            begin
                `uvm_info(this.get_name(), "Hello UVM Test::Monitor::Start", UVM_NONE);
                forever begin
                    exp = vif.in_a + vif.in_b;
                    `uvm_info(this.get_name(),
                              $sformatf("exp %d, act %d", exp, vif.out_s), UVM_LOW);
                    @vif.clock;
                end
                `uvm_info(this.get_name(), "Hello UVM Test::Monitor::Finish", UVM_NONE);
            end
        join
    endtask

endclass // test
