// -*- coding: utf-8 -*-

module adder
#(parameter DATA_WIDTH = 8)
(
    input  logic clock,
    input  logic reset_n,
    input  logic [DATA_WIDTH-1:0] in_a,
    input  logic [DATA_WIDTH-1:0] in_b,
    output logic [DATA_WIDTH-1:0] out_s,
    output logic [DATA_WIDTH-1:0] out_c
);

    localparam D1 = 1;
    logic [DATA_WIDTH:0] w_sum;
    logic [DATA_WIDTH:0] r_sum;

    assign {out_c, out_s} = r_sum;
    assign w_sum = in_a + in_b;

    always_ff @ (posedge clock, negedge reset_n) begin
        if (~reset_n) begin
            r_sum <= #D1 '0;
        end
        else begin
            r_sum <= #D1 w_sum;
        end
    end

endmodule
