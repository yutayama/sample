// -*- coding: utf-8 -*-

interface adder_if
#(parameter DATA_WIDTH = 8)
(
    input  logic clock,
    input  logic reset_n
);

    logic [DATA_WIDTH-1:0] in_a;
    logic [DATA_WIDTH-1:0] in_b;
    logic [DATA_WIDTH-1:0] out_s;
    logic [DATA_WIDTH-1:0] out_c;

endinterface
