// -*- coding: utf-8 -*-

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "testbench.svh"

module tb;

    logic clock;
    logic reset, reset_n;
    clock_gen cg(.*);
    reset_gen rg(.*);
    adder_if vif(.*);
    adder dut (
        .in_a (vif.in_a ),
        .in_b (vif.in_b ),
        .out_s(vif.out_s),
        .out_c(vif.out_c),        
        .*
    );

    initial begin
        uvm_config_db #(virtual adder_if)::set(null, "*", "vif", vif);
    end

    initial begin
        run_test("test"); // default
    end

    localparam TIMEOUT = 10000;
    initial begin
        #TIMEOUT;
        $display("timeout");
        $finish;
    end

endmodule // tb
