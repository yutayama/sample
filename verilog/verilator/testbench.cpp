// -*- coding: utf-8; mode: c++ -*-

#include <cstdio>
#include <iostream>
#include <verilated.h>

#include "Vbuffer.h"

int main(int argc, char** argv)
{
    Verilated::commandArgs(argc, argv);
    
    // instantiate dut
    Vbuffer* dut = new Vbuffer();
    // initialize input signals
    dut->clock = 0;
    dut->reset_n = 1;
    dut->wr_valid = 0;
    dut->wr_data = 0;
    dut->rd_valid = 0;

    static const int TIMEOUT = 1000;
    static const int CLOCK_PERIOD = 10;

    for (int i = 0; i < 100; i++) {
        dut->eval();
    }
    printf("Start Verilaor Simulation");

    int clock = 0;
    for (int time = 0; time < TIMEOUT; time++) {
        if ((time % CLOCK_PERIOD/2) == 0) {
            dut->clock = !dut->clock;
        }
        if (clock == 3) {
            dut->reset_n = 0;
        }
        if (clock == 5) {
            dut->reset_n = 1;
        }
        dut->eval();
        if ((time % CLOCK_PERIOD) == 0) {
            printf("[%05d]::rst(%d), wr(%d, %d, %d), rd(%d, %d, %d)\n",
                    clock, dut->reset_n,
                    dut->wr_valid, dut->wr_ready, dut->wr_data,
                    dut->rd_valid, dut->rd_ready, dut->rd_data);
            clock++;
        }
    }

    printf("Finish Verilaor Simulation");
    return 0;
}