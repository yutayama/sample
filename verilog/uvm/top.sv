// -*- coding: utf-8 -*-

module top;
    // UVM の macro と package を使えるようにする. 
    `include "uvm_macros.svh"
    import uvm_pkg::*;

    class test000 extends uvm_test;  // 一番上はuvm_testを継承したclassを使用する。
        `uvm_component_utils(test000)  // sample_testをuvmのデータベースに登録する。 

        // systemverilogのクラスの構文。uvm独自ではない
        function new(string name="test000", uvm_component parent=null);
            super.new(name, parent);
        endfunction

        // uvm独自の構文
        task run_phase(uvm_phase phase);
            `uvm_info(this.get_name(), "Hello UVM World::Test000", UVM_NONE);
        endtask
    
    endclass

    class test001 extends uvm_test;  // 一番上はuvm_testを継承したclassを使用する。
        `uvm_component_utils(test001)  // sample_testをuvmのデータベースに登録する。 

        // systemverilogのクラスの構文。uvm独自ではない
        function new(string name="test000", uvm_component parent=null);
            super.new(name, parent);
        endfunction

        // uvm独自の構文
        task run_phase(uvm_phase phase);
            `uvm_info(this.get_name(), "Hello UVM World::Test001", UVM_NONE);
        endtask
    
    endclass

    initial begin
        // run_test("test000"); // `uvm_component_utils(<クラス>)で登録した<クラス>を実行する。
        run_test(); // `uvm_component_utils(<クラス>)で登録した<クラス>を実行する。
    end

endmodule // top
