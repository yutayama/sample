# -*- coding: utf-8 -*-

from jinja2 import Template

verilog_module = """\
module {{ name }} ({{ ports }});
endmodule : {{ name }}
"""

template = Template(verilog_module)
s = template.render(name='top', ports='clocn, reset_n')
print(s)

