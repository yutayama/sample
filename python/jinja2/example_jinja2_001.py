# -*- coding: utf-8 -*-

import jinja2
import json

env = jinja2.Environment(loader=jinja2.FileSystemLoader('./', encoding="utf-8"))
template = env.get_template('template.j2')
#template = env.get_template('foo.j2')

with open('parameter.json') as f:
    params = json.load(f)

s = template.render(params)
print(s)

