#!/usr/bin/env python
#-*- coding: utf-8 -*-

from pathlib import Path

p = Path('..')
print(p)
print(p.resolve().as_posix())

p.with_name('foo.bar')
p.with_stem('foo')
p.with_suffix('.txt')
