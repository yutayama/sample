# -*- coding: utf-8 -*-

from collections import OrderedDict

class MyDict:
    
    def __init__(self, *args, **kwargs):
        self._d = OrderedDict()
        self._iter = None

    def __iter__(self):
        self._iter = iter(self._d)
        return self._iter
    
    def keys(self):
        self._iter = self._d.keys()
        return self._iter

    def values(self):
        self._iter = self._d.values()
        return self._iter

    def items(self):
        self._iter = self._d.items()
        return self._iter
    
    def __next__(self):
        return next(self._iter)
        
    def append(self, key, value):
        self._d[key] = value
        

def test():
    d = MyDict()
    d.append(0, 'a')
    d.append('x', 'b')
    d.append(1, 'c')

    print('for::')    
    for i in d:
        print(i)

    print('for_keys::')
    for i in d.keys():
        print(i)

    print('for_values::')
    for i in d.values():
        print(i)

    print('for_items::')
    for k, v in d.items():
        print(k, v)

        
if __name__ == "__main__":
    test()
