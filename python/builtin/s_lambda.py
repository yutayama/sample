# -*- coding: utf-8 -*-

def add(x, y):
    z = x + y
    return z

lambda_add = lambda x, y: x + y

if __name__ == "__main__":
    if add(1, 2) != 3:
        print("FAIL1-1")
        quit()
    if lambda_add(1, 2) != 3:
        print("FAIL1-2")
        quit()
    print("PASS1")

    a = [0, 1, 2, 3]
    b = list(map(lambda x: x**2, a))
    if b == [0, 1, 4, 9]:
        print("PASS2")
    else:
        print(b)
