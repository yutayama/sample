# -*- coding: utf-8 -*-

from re import T


class Sample:
    
    def __init__(self) -> None:
        pass
    
    def __str__(self) -> str:
        return f"{self.__class__.__name__}"
    
    def __reduce__(self) -> str | tuple[Any, ...]:
        return (self.__class__, (self))
    
    def __eq__(self, other: 'Sample') -> bool:
        return True
    
    def __ne__(self, other: 'Sample') -> bool:
        return False
    
