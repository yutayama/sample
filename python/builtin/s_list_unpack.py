# -*- coding: utf-8 -*-

a = [i for i in range(10)]
print("[packed list]   a =", a)
print("[unpacked list]*a =", *a)