import torch
from torch import Tensor

def select():
    '''
    要素の各行ごとにindexを指定して抽出
    '''
    a = Tensor(range(12)).reshape(3, 4)
    b = Tensor([1, 3, 2])
    a.gather(1, b.reshape(3, 1))
    # tensor([[ 1.],
    #         [ 7.],
    #         [10.]])

def argmax():
    a = Tensor(range(12)).reshape(3, 4)
    a.argmax(1)    

if __name__ == "__main__":
    select()