import torch
from torch import nn
from torch import Tensor
from torch.nn import functional as F

class QNetwork(nn.Module):
    '''A simple QNetwork class'''
    
    def __init__(self, n_state, n_action) -> None:
        super().__init__()
        self.n_state = n_state
        self.n_action = n_action
        self.n_hidden = 8
        self.ln0 = nn.Linear(n_state, self.n_hidden)
        self.ln1 = nn.Linear(self.n_hidden, self.n_hidden)
        self.ln2 = nn.Linear(self.n_hidden, n_action)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        print(x.shape)
        x = F.relu(self.ln0(x))
        print(x.shape)
        x = F.relu(self.ln1(x))
        print(x.shape)
        x = F.relu(self.ln2(x))
        print(x.shape)
        return x    

def run000():
    model = QNetwork(n_state=4, n_action=1)
    t0 = Tensor(range(4))
    t1 = Tensor(range(4)).reshape(4, 1)
    t2 = Tensor(range(8)).reshape(4, 2)
    
    print("Run000")
    z = model(t0)
    print(f"in = {t0}, out = {z}")
    print("Run001 -> FAIL:RuntimeError: mat1 and mat2 shapes cannot be multiplied (4x1 and 4x8)")
    z = model(t1)
    print(f"in = {t0}, out = {z}")
    print("Run002 -> FAIL:RuntimeError: mat1 and mat2 shapes cannot be multiplied (4x1 and 4x8)")
    z = model(t2)
    print(f"in = {t1}, out = {z}")

def run001():
    ''' Linear Module は tensor[-1] を入力のベクタ長に合わせる必要がある '''
    model = QNetwork(n_state=4, n_action=1)
    t0 = Tensor(range(4))
    t1 = Tensor(range(4)).reshape(1, 4)
    t2 = Tensor(range(8)).reshape(2, 4)
    
    print("Run000")
    z = model(t0)
    print(f"in = {t0}, out = {z}")
    print("Run001 -> PASS")
    z = model(t1)
    print(f"in = {t0}, out = {z}")
    print("Run002 -> PASS")
    z = model(t2)
    print(f"in = {t1}, out = {z}")
    
if __name__ == "__main__":
    # run000()
    run001()
    