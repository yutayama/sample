#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('in', nargs='?')
parser.add_argument('out', nargs='?')
parser.add_argument('opt', nargs='*')
parser.add_argument('opt2', nargs=1)
parser.add_argument('-l', '--library', type=str, default='lib')
parser.add_argument('-f', '--flag', action='store_true')
args = parser.parse_args()

print(args)
