#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('-l', '--library', type=str, default='lib')
parser.add_argument('-f', '--flag', action='store_true')
subparsers = parser.add_subparsers(help='sub command help')

parser_a = subparsers.add_parser('a', help='sub command a')
parser_a.add_argument('var', type=str, default='ls')
parser_a.add_argument('-x', '--execute', type=str, default='ls')

parser_b = subparsers.add_parser('b', help='sub command a')
parser_b.add_argument('var', type=str, default='ls')
parser_b.add_argument('-f', '--flag', action='store_true')

'''
action
    - store: 引数を保持
      - store_const: フラグが指定されたら 'const'で指定した値を保持 (引数はなし)
        - store_true: フラグが指定されたら, Tureを保持 (引数はなし)
        - store_false: フラグが指定されたら, Falseを保持 (引数はなし)
    - append: 複数回指定した場合の引数をリストとして保持
    - append_const
    - count: 引数を指定した回数を保持 (ps auxxxx) の xの回数とか
'''
args = parser.parse_args()

print(args)
