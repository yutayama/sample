# -*- coding: utf-8 -*-

def disp_logger(logger, num=0):
    logger.critical('#{}'.format(num))
    logger.critical('critical')
    logger.error('error')
    logger.warning('warning')
    logger.info('info')
    logger.debug('debug')

logger = getLogger(__name__)
disp_logger(logger, 1)

logger.setLevel(DEBUG)
disp_logger(logger, 2)

sh = StreamHandler() # default is sys.stdout
sh.setLevel(DEBUG)
sh_fmt = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
sh.setFormatter(sh_fmt)
logger.addHandler(sh)
disp_logger(logger, 3)

import sys
fh = FileHandler('./error.log')
fh.setLevel(WARNING)
fh_fmt = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(fh_fmt)
logger.addHandler(fh)
disp_logger(logger, 4)

logger.setLevel(CRITICAL)
disp_logger(logger, 5)
