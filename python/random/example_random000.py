# -*- coding: utf-8 -*-

import random


def example_rand():

    a = [random.randrange(0, 10) for i in range(10)]
    print(a)

    a = tuple(a)
    print(type(a))
    for _ in range(10):
        b = random.sample(a, 1)
        print(b, type(b))

def example_rand2d():

    a = list()
    for i in range(10):
        a.append([random.randrange(0, 10) for i in range(2)])
    print(a)

    a = tuple(a)
    print(type(a))
    for _ in range(10):
        b = random.sample(a, 2)
        print(b, type(b))
        
if __name__ == "__main__":
    example_rand()