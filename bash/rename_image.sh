#!/bin/bash

# 画像ファイル(jpg, png, heif) を exif DateTimeOriginal の時刻を IMG_`date +"%Y%m%d_%H%M%S"`.{jpg,png,heif} にrename

filelist=$(find ./ -regex ".*\.\(jpg\|JPG\|jpeg\|JPEG\|png\|PNG\|heic\|HEIC\|heif\|HEIF\)$")

for i in $filelist ; do
    suffix=${i##*.}
    exif_date=$(exiftool -T -datetimeoriginal $i | sed -e 's/://g' -e 's/ /_/g')
    stem=IMG_${exif_date}
    if [ "$exif_date" != "-" ]; then
        echo mv $i ${stem}.${suffix}
    else
        echo $i is not renamed.
    fi
done
